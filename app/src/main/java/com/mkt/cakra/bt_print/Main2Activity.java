package com.mkt.cakra.bt_print;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;
import android.os.Environment;
import android.os.Bundle;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

public class Main2Activity extends Activity  {
    protected static final String TAG = "TAG";
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    byte FONT_TYPE;
    Button mScan, mPrint, mDisc;
    BluetoothAdapter mBluetoothAdapter;
    private UUID applicationUUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ProgressDialog mBluetoothConnectProgressDialog;
    BluetoothDevice mBluetoothDevice;
    private  String BILL;
    private String urlx;
    private String KEY_NAME="URLX";
    private WebView browser;
    private String url="http://10.146.90.226/ecafe/struk.html";
    private static final int REQUEST_CODE_PERMISSION = 2;
    private String urlb;
    private Bitmap bitmap;
    private static BluetoothSocket btsocket;
    private static OutputStream outputStream;

    private PrintManager printManager;
    private PrintDocumentAdapter printAdapter;
    private String jobName;

    private void createWebPrintJob(WebView webView) {
        printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);
        printAdapter = webView.createPrintDocumentAdapter();
        jobName = getString(R.string.app_name) + " Document";
        //PrintJob printJob = printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());

    }
    public static Bitmap captureWebViewLong(WebView webView) {
        String path = Environment.getExternalStorageDirectory()
                .toString();

        Bitmap b = null;
        try {
            Picture picture = webView.capturePicture();
            b = Bitmap.createBitmap(
                    picture.getWidth(), picture.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            picture.draw(c);
            File file = new File(path, "/struk.png");

            if (file.exists()) {
                file.delete();
            }
            FileOutputStream fos = new FileOutputStream(new File(Environment.getDataDirectory().toString() + "/struk.png"));

            //FileOutputStream fos = new FileOutputStream(file.getAbsoluteFile());
            if (fos != null) {
                b.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras=getIntent().getExtras();
        urlx=extras.getString(KEY_NAME);
        urlb=urlx;
        setContentView(R.layout.activity_main2);
        browser = (WebView) findViewById(R.id.browser);

        browser.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                browser.measure(View.MeasureSpec.makeMeasureSpec(
                        View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                browser.layout(0, 0, browser.getMeasuredWidth(),
                        browser.getMeasuredHeight());
                browser.setDrawingCacheEnabled(true);
                browser.buildDrawingCache();
                Bitmap bm = Bitmap.createBitmap(browser.getMeasuredWidth(),
                        browser.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

                Canvas bigcanvas = new Canvas(bm);
                Paint paint = new Paint();
                int iHeight = bm.getHeight();
                bigcanvas.drawBitmap(bm, 0, iHeight, paint);
                browser.draw(bigcanvas);
                System.out.println("1111111111111111111111="
                        + bigcanvas.getWidth());
                System.out.println("22222222222222222222222="
                        + bigcanvas.getHeight());

                if (bm != null) {
                    try {
                        String path = Environment.getExternalStorageDirectory()
                                .toString();
                        File file = new File(path, "/struk.png");
                        if (file.exists()) {
                            file.delete();
                        }
                        OutputStream fOut = null;

                        fOut = new FileOutputStream(file);

                        bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                        fOut.flush();
                        fOut.close();
                        bm.recycle();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        WebSettings settings = browser.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        settings.setDisplayZoomControls(false);
        settings.setDefaultTextEncodingName("UTF-8");
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setDomStorageEnabled(true);


        browser.loadUrl(url);

        mScan = (Button) findViewById(R.id.button);
        mScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View mView) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        mPrint = (Button) findViewById(R.id.button2);
        mPrint.setOnClickListener(new View.OnClickListener() {
            public void onClick(View mView) {


                printbill();
            }
        });
    }
    protected void printbill() {
        if(btsocket == null){
            Intent BTIntent = new Intent(getApplicationContext(), DeviceList.class);
            this.startActivityForResult(BTIntent, DeviceList.REQUEST_CONNECT_BT);
        }
        else{
            OutputStream opstream = null;
            try {
                opstream = btsocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            outputStream = opstream;
            try {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                outputStream = btsocket.getOutputStream();

                byte[] printformat = { 0x1B, 0*21, FONT_TYPE };
                printUnicode();
                cetakBill();
                printNewLine();
                printUnicode();
                printNewLine();
                printNewLine();

                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void cetakBill() {
        String path = Environment.getExternalStorageDirectory()
                .toString();
        File file = new File(path, "/struk.png");
        try {
            Bitmap bmp = BitmapFactory.decodeFile(file.toString());
            if(bmp!=null){
                byte[] command = Utils.decodeBitmap(bmp);
                outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                printText(command);
            }else{
                Log.e("Cetak Gagal", "Render PNG gagal");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Cetak Gagal", "Tidak terhubung dengan printer");
        }
    }
    public void printUnicode(){
        try {
            outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
            printText(Utils.UNICODE_TEXT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void printNewLine() {
        try {
            outputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void printText(byte[] msg) {
        try {
            outputStream.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void printText(String msg) {
        try {
            outputStream.write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        try {
            if (btsocket != null)
                btsocket.close();
        } catch (Exception e) {
            Log.e("Tag", "Exe ", e);
        }
    }




}