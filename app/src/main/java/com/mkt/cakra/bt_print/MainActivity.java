package com.mkt.cakra.bt_print;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebView;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private String url = "http://10.146.90.226/ecafe/app.php?hal=penjualan"; //sesuikan urlnya
    private String TAG = this.getClass().getSimpleName();
    private String KEY_NAME = "URLX";
    private String urlx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initializeWebView() {
        if (getIntent().getStringExtra("url") != null){
            url = getIntent().getStringExtra("url");
        }
        Log.d(TAG, "initializeWebView: " + url);
        webView = (WebView) findViewById(R.id.webView);

        webView.loadUrl(url);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());


    }

    public class WebChromeClient extends android.webkit.WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);}
            //progressBar.setProgress(newProgress);

    }

    public class WebViewClient extends android.webkit.WebViewClient {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if (url != null && url.startsWith("http://10.146.90.226/ecafe/struk.html")) {
                        try
                        {

                            Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                            intent.putExtra(KEY_NAME, url);
                            startActivity(intent);

                        }catch(Exception e)
                        {
                            e.printStackTrace();
                        }

                    return true;}

                else{
                    return false;
                }
            }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)  {
            super.onPageStarted(view, url, favicon);
            //progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            //progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()){
            webView.goBack();
        } else {
            showAlertDialog();
        }
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage("Tutup Aplikasi ini ?")
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}